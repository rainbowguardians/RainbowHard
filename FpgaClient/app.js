/* jshint unused: true */
/* jslint vars: true */

var net = require('net');
var sha1 = require('sha1');
var config = require('./config');

var socket = new net.Socket();
socket.setTimeout(100000)

var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var sharedBody;
var chosedOption;
var passArray = [];
var currentPassword;
var chain = {
    beginPass: null,
    endPass: null
};
var startTime;
var stopTime;

rl.setPrompt('Choose what to do:\nHARDWARE\n1. Connect the FPGA\n2. Send data to FPGA\n3. Close FPGA connection\nSOFTWARE\n4. Software sha1-reduction iterations ('+config.sharedIter+')\n5. Generate passwords\n6. Compute passwords\n7. Set iterations number\n');
rl.prompt();

rl.on('line', (line) => {
    chosedOption = line.trim();
  switch(chosedOption) {
    case '1':
        socket.connect({port:config.localport,host:config.localhost},function (err) {
            console.log('Connection established');           
        });
      break;
    case '2':
        rl.question('Write password to send (max 8 letters): ', (data) => {                      
            socket.write(Buffer.from(data+'\n','ascii'),function () {
                startTime = new Date().getTime();
                console.log('Password sent');
            }); 
        });        
        break;
    case '3':
        socket.write(Buffer.from('q\n','ascii'),function () {
            console.log('Password sent');
        }); 
        break;
    case '4':
        rl.question('Is password in (a)scii or (h)ex?: ', (ans) => {
            rl.question('Write password to compute (max 8 letters): ', (data) => {
                rl.question('Write number of password iterations: ', (dataIter) => {
                    config.sharedIter = parseInt(dataIter);
                    if(ans == 'h') {data = hex2a(data)} 
                    red = shared(data);
                    console.log(red);
                });
            });
        });
        break;   
    case '5':
        rl.question('Write number of passwords to generate: ', (data) => {
            passArray = [];
            config.passNumber = parseInt(data);
            var i=0;
            for(i;i<config.passNumber;i++){
                chain.beginPass = Math.floor(Math.random()*90000000);
                passArray.push(chain);
            }
            console.log('Passwords generated');
        });
        break;
    case '6':
        console.log('Computing started...');
        startTime = new Date().getTime();
        var i = 0;
        for(i;i<config.passNumber;i++){            
            passArray[i].endPass = shared(passArray[i].beginPass);
        }
        stopTime = new Date().getTime();
        console.log('Computing finished. Time in [ms]: '+(stopTime-startTime));
        break;        
    case '7':
        rl.question('Write number of iterations: ', (data) => {
            config.sharedIter = parseInt(data);
            rl.setPrompt('Choose what to do:\nHARDWARE\n1. Connect the FPGA\n2. Send data to FPGA\n3. Close FPGA connection\nSOFTWARE\n4. Software sha1-reduction iterations ('+config.sharedIter+')\n5. Generate passwords\n6. Compute passwords\n7. Set iterations number\n');
            console.log('Iterations set');
        });
        break;
    default:
        console.log('Say what? I might have heard `' + line.trim() + '`');
        break;
  }  
  rl.prompt();
}).on('close', () => {
    console.log('Have a great day!');
    process.exit(0);
});

socket.on('data',(data) => {    
    if(chosedOption == 2) {
        stopTime = new Date().getTime();
        sharedBody = JSON.parse(data);
        // console.log(sharedBody.pass);
        console.log(hex2a(sharedBody.red));
        console.log('Computing finished. Time in [ms]: '+(stopTime-startTime));
    } else {
        console.log(data.toString());
    }
});

socket.on('end', (data) => {
    console.log('Connection ended');
});

socket.on('error', (data) => {
    console.log('Connection error: '+data);
});

function reduction(hash) {
    return hash.slice(0,5);
}

function hex2a(hexx) {
    var hex = hexx.toString();
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function ascii_to_hexa(str) {  
    var arr1 = [];  
    for (var n = 0, l = str.length; n < l; n ++)   
        {  
        var hex = Number(str.charCodeAt(n)).toString(16);  
        arr1.push(hex);  
        }  
    return arr1.join('');  
}  

function shared(password) {
    var i=0;
    var hash;
    var red = password;
    for(i;i<config.sharedIter;i++){
        hash = sha1(red);
        red = reduction(hash);
    }
    return red;
}