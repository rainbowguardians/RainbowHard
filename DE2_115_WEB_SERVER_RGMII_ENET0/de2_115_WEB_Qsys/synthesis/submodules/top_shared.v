module top_shared(
	input 			clk,
	input				clk_en,
	input 			reset,
	input 			start,
	input [7:0]	n,
	input [31:0] 	msg1, msg2,
//	output[39:0]	out_red,
	output[31:0]	result,
	//output 			done_red
	output reg final_done
);
reg start_ready;
reg [31:0] out_reg;
wire    done_red, done_red2, done_sha, start_red, start_sha;
wire [31:0] msg1_sha, msg2_sha;
wire [159:0] 	hash;
wire [31:0] out_red1, out_red2;

assign start_red = done_sha;
assign start_sha = start || done_red2;

assign msg1_sha = start? msg1: out_red1; 
assign msg2_sha = start? msg2: out_red2;
assign done_red2 = done_red && start_ready;

reg[15:0] counter;

always@ (posedge clk) begin
	if (reset) counter <= 0;
	else begin
		if (start) start_ready = 1;
		if (counter == 16'd9999) begin start_ready<=0; end
		else if(done_red==1 && start_ready==1 ) counter <= counter + 1'b1; 
		else final_done <= 0;
	end
	if (done_red==1 && done_red2==0 && start_sha==0 && counter == 16'd9999) 
	begin final_done<=1; counter <=0; end
	case (n)
	6: begin out_reg = out_red2; final_done <= 1; counter <= 0; start_ready<=0; end
	5: begin out_reg = out_red1; end
	endcase
end

assign result = out_reg;

sha1sm sha1sm_inst(
	.clk			(clk),
	.clk_en		(clk_en),
	.rst			(reset),
	.start		(start_sha),
	.msg1			(msg1_sha),
	.msg2			(msg2_sha),
	.hash			(hash),
	.done			(done_sha)
);

reduct reduct_inst(
	.clk			(clk),
	.clk_en		(clk_en),
	.rst			(reset),
	.start		(start_red),
	.hash			(hash),
	.out_red		(out_red1),
	.out_red2		(out_red2),
	.done			(done_red)
);

endmodule