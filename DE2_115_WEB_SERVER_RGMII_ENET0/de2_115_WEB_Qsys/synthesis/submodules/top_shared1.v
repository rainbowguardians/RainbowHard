module top_shared(
	input 			clk,
	input 			reset,
	input 			start,
	input [31:0] 	msg1, msg2,
//	output[39:0]	out_red,
	output[31:0]	out_red,
	//output 			done_red
	output reg final_done
);
reg start_ready;
wire    done_red, done_red2, done_sha, start_red, start_sha;
wire [31:0] msg1_sha, msg2_sha;
wire [159:0] 	hash;
assign start_red = done_sha;
assign start_sha = start || done_red2;

assign msg1_sha = start? msg1: out_red; 
assign msg2_sha = start? msg2: 0;
assign done_red2 = done_red && start_ready;

reg[13:0] counter;

always@ (posedge clk) begin
	if (reset) counter <= 0;
	else if (start) start_ready = 1;
	else if (done_red==1) counter <= counter + 1'b1;
	if (counter == 16'd1) begin final_done <= 1; counter <= 0; start_ready<=0; end
	else final_done <= 0; 
end

sha1sm sha1sm_inst(
	.clk			(clk),
	.rst			(reset),
	.start		(start_sha),
	.msg1			(msg1_sha),
	.msg2			(msg2_sha),
	.hash			(hash),
	.done			(done_sha)
);

reduct reduct_inst(
	.clk			(clk),
	.rst			(reset),
	.start		(start_red),
	.hash			(hash),
	.out_red		(out_red),
	.done			(done_red)
);

endmodule