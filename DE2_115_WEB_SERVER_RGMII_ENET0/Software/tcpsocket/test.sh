#!/bin/sh
inp="tcpsocket"
oup="tcpsocket_out"
nios2-elf-size ${inp}.elf > ${oup}.size.txt
nios2-elf-readelf -h ${inp}.elf > ${oup}.program.txt
nios2-elf-objdump -dS ${inp}.elf > ${oup}.da.txt
echo done