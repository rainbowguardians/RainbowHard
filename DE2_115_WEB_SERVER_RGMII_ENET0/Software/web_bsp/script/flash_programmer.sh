#!/bin/sh
#
# This file was automatically generated.
#
# It can be overwritten by nios2-flash-programmer-generate or nios2-flash-programmer-gui.
#

#
# Converting Binary File: C:\Users\kamil\Desktop\DE2_115_WEB_SERVER_RGMII_ENET0\Software\web\system\ro_zipfs.zip to: "..\flash/ro.flashfs_cfi_flash.flash"
#
bin2flash --input="C:/Users/kamil/Desktop/DE2_115_WEB_SERVER_RGMII_ENET0/Software/web/system/ro_zipfs.zip" --output="../flash/ro.flashfs_cfi_flash.flash" --location=0x100000 

#
# Programming File: "..\flash/ro.flashfs_cfi_flash.flash" To Device: cfi_flash
#
nios2-flash-programmer "../flash/ro.flashfs_cfi_flash.flash" --base=0x9000000 --sidp=0xB000140 --id=0x0 --timestamp=1464045572 --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program 

