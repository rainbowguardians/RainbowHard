module sha1sm(
input clk,
input clk_en,
input rst,
input start,
input [31:0] msg1, msg2,
output reg [159:0] hash,
output reg done
);
reg ready_2, ready_3, ready_4;
reg [3:0] i, next_i;
reg [5:0] state, next_state; 
parameter idle=6'd1, load=6'd2, pad=6'd4, schedule16=6'd8, schedule80=6'd16, out=6'd32;
reg [6:0] vari, next_vari, t, next_t;
reg [31:0] h0, h1, h2, h3, h4, a, b, c, d, e, next_a, next_b, next_c, next_d, next_e, word, next_word; 
reg [31:0]  Kt, next_Kt;
//reg [32:0] T, next_T; ft, next_ft,
reg [63:0] msg_r;
reg [511:0] padded, next_padded;
wire [63:0] msg;

function reg inbetween(input [6:0] low, value, high); 
begin
  inbetween = value >= low && value <= high;
end
endfunction

assign msg = {msg2, msg1};

always @(posedge clk) begin
	/*if (rst) begin
		msg_r <= 64'b0;
		padded <= 512'b0;
		ready_2 <= 1'b0; ready_3 <= 1'b0; ready_4 <= 1'b0;
		//initial hash
		h0<=32'h67452301; h1<=32'hefcdab89; h2<=32'h98badcfe ; h3<=32'h10325476; h4<=32'hc3d2e1f0; 
		//next_a<=32'h67452301; next_b<=32'hefcdab89; next_c<=32'h98badcfe ; next_d<=32'h10325476; next_e<=32'hc3d2e1f0; 
		i <= 3'd0;
		t <= 7'd0; 
		vari = 1'b0;
	end
	else begin*/
	state <= next_state; 
	a <= next_a; b <= next_b; c <= next_c; d <= next_d; e <= next_e;
	t <= next_t; vari <= next_vari; i <= next_i; 
	word <= next_word; padded <= next_padded; Kt <= next_Kt; // T <= next_T;ft <= next_ft
		case (state)
		idle: begin
			h0<=32'h67452301; h1<=32'hefcdab89; h2<=32'h98badcfe ; h3<=32'h10325476; h4<=32'hc3d2e1f0; 
			next_a <= 32'h67452301; next_b <= 32'hefcdab89; next_c <= 32'h98badcfe; next_d <= 32'h10325476; 
			next_e <= 32'hc3d2e1f0;
			msg_r <= msg;
			next_padded <= 512'b0;
			ready_3 <= 1'b0;
			ready_2 <= 1'b0;
			ready_4 <= 1'b0;
			done <= 1'b0; 
			next_i <= 4'd8;
			next_t <= 6'd0;
			next_vari <= 1'b0;
			next_Kt <= 32'h5a827999;

			end
	//	load:	begin
	//		msg_r <= 
			//next_ft <= (b&c)^(~b&d);
			//next_word <= msg[63 : 32];
		//end
		pad: begin
			next_i <= i-1'b1; 
			if (msg_r [i*8-1-:8] != 0) begin
				next_padded [511-vari*8-:8] <= msg_r [i*8-1-:8];
				next_vari <= vari+1'b1;
			end	
		 if (i==1'b1) begin 
				next_padded [6:0] <= (vari+1'b1)<<3;
				next_vari <= 7'd14;
				next_padded [511-(vari+1'b1)*8] <= 1'b1; 
			end
		 if (i==1'b0) begin 
			ready_2 <= 1'b1; // ?
			next_word <= padded[511:480];
			end
		end
		schedule16: begin
			if (t==7'd14) ready_3 <= 1'b1;
			next_word <= padded[32*vari+31 -: 32]; 
			next_t <= t+1'b1;
			next_vari <= vari - 1'b1;	
			next_e <= d;
		   next_d <= c;	
			next_c <= {b[1:0], b[31:2]};
			next_b <= a;
			//next_a <= ft + a2;
			next_a <= ({a[26:0],a[31:27]}) + ((b&c)^(~b&d)) + e + Kt + word; // + ft + e + Kt
		end
		schedule80: begin
			next_word [31:1] <= (padded[510 : 480]^padded[446 : 416])^(padded[254 : 224]^padded[94 : 64]); 
			next_word [0] <= (padded[511]^padded[447])^(padded[255]^padded[95]);
			next_padded <= {padded[479:0],(padded[510 : 480]^padded[446 : 416])^(padded[254 : 224]^padded[94 : 64]), (padded[511]^padded[447])^(padded[255]^padded[95])};
			next_t <= t+1'b1; 
			next_e <= d;
		   next_d <= c;	
			next_c <= {b[1:0], b[31:2]};
			next_b <= a;
			case (1) // 
				inbetween(7'd0,t,7'd18): 	next_Kt <= 32'h5a827999;
				inbetween(7'd19,t,7'd38) : next_Kt <= 32'h6ed9eba1;
				inbetween(7'd39,t,7'd58) : next_Kt <= 32'h8f1bbcdc; 
				inbetween(7'd59,t,7'd89) : next_Kt <= 32'hca62c1d6; 
				default :next_Kt <= 32'h5a827999;
			endcase
			case (1) // 
				inbetween(7'd0,t,7'd19): 	next_a <= ({a[26:0],a[31:27]}) + ((b&c)|(~b&d)) + e + Kt + word;
				inbetween(7'd20,t,7'd39) : next_a <= ({a[26:0],a[31:27]}) + (b^c^d) + e + Kt + word; 
				inbetween(7'd40,t,7'd59) : next_a <= ({a[26:0],a[31:27]}) + ((b&c)^(b&d)^(c&d)) + e + Kt + word;
				inbetween(7'd60,t,7'd89) : next_a <= ({a[26:0],a[31:27]}) + (b^c^d) + e + Kt + word;
				default :next_a <= ({a[26:0],a[31:27]}) + ((b&c)^(~b&d)) + e + Kt + word;
			endcase
			if (t==7'd79) 
			ready_4 <= 1'b1; 		
		end
		out : begin
			hash [159 : 128] <= a+h0;
			hash [127 : 96] <= b+h1;
			hash [95 : 64] <= c+h2;
			hash [63 : 32] <= d+h3;
			hash [31 : 0] <= e+h4;
			done <= 1'b1;
		end
		endcase
end	


always @* begin
	if(rst)
	next_state = idle;
	else 
	case (state) 
	idle: next_state = start ? pad : idle;
	load: next_state = pad;// ready_2 ? schedule16 : pad; //
   pad: next_state = ready_2 ? schedule16 : pad;
	schedule16: next_state = ready_3 ? schedule80 : schedule16;
	schedule80: next_state = ready_4 ? out : schedule80;
	out: next_state = idle;
	default: next_state = idle;
	endcase
end

endmodule