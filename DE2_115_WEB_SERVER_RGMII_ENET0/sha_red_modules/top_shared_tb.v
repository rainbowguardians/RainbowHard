`timescale 1 ns / 100 ps
module top_shared_tb(
output [31:0] testout,
output final_done
);

reg clk, rst, start;
reg [31:0] msg1;
reg [31:0] msg2;
reg [7:0] n;

//wire [31:0] out;

/*event wait_for_ready;
	event done;
	initial begin 
		forever begin 
      @ (ready); 
      -> done; 
    end 
  end*/
  
initial begin
	clk = 0;
	rst = 0;
	n = 5;
	msg1 = "abcd";
	msg2 = "dcb";
	start = 0;
	#50 rst=1;
	#50 rst =0;
	#150 start=1;
	#50 start =0;
	#190000000 n = 6;
	#10000 $stop(1);
end

always #50 clk = !clk; 

top_shared UUT(
.clk(clk),
.reset(rst),
.start(start),
.n(n),
.msg1(msg1),
.msg2(msg2),
.result(testout),
.final_done(final_done)
);

endmodule