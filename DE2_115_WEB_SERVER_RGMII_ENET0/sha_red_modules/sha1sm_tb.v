`timescale 1 ns / 100 ps
module sha1sm_tb(
output [159:0] testout,
output done
);

reg clk, rst;
reg start;
reg [31:0] msg1;
reg [31:0] msg2;
//wire [31:0] out;

/*event wait_for_ready;
	event done;
	initial begin 
		forever begin 
      @ (ready); 
      -> done; 
    end 
  end*/
  
initial begin
	clk = 0;
	rst = 0;
	start = 0;
	msg1 = "abcd";
	msg2 = "efg";
	rst = #200 1;
	rst = #200 0;

	start = #200 1;
	start = #200 0;
	@(done);
	#5000 $stop(1);
end

always #50 clk = !clk; 

sha1sm UUT(
.clk(clk),
.rst(rst),
.start(start),
.msg1(msg1),
.msg2(msg2),
.hash(testout),
.done(done)
);
endmodule