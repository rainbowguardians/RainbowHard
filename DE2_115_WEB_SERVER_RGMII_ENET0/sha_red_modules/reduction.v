module reduct(clk,clk_en,rst,start,done,hash,out_red,out_red2);
input clk;
input clk_en;
input rst;
input start;
input [159:0] hash;
//output reg [39:0] out_red;
output reg [31:0] out_red;
output reg [31:0] out_red2;
output reg done;
//output reg [15:0] R1;
//output reg [15:0] R2;
//output reg [7:0] R3;

//reg [39:0] plain;


always @(posedge clk) begin
if (start) begin
case (hash[159:156])
0: out_red2[7:0]<=8'D48;
1: out_red2[7:0]<=8'D49;
2: out_red2[7:0]<=8'D50;
3: out_red2[7:0]<=8'D51;
4: out_red2[7:0]<=8'D52;
5: out_red2[7:0]<=8'D53;
6: out_red2[7:0]<=8'D54;
7: out_red2[7:0]<=8'D55;
8: out_red2[7:0]<=8'D56;
9: out_red2[7:0]<=8'D57;
10: out_red2[7:0]<=8'D97;
11: out_red2[7:0]<=8'D98;
12: out_red2[7:0]<=8'D99;
13: out_red2[7:0]<=8'D100;
14: out_red2[7:0]<=8'D101;
15: out_red2[7:0]<=8'D102;
endcase

case (hash[155:152])
0: out_red[31:24]<=8'D48;
1: out_red[31:24]<=8'D49;
2: out_red[31:24]<=8'D50;
3: out_red[31:24]<=8'D51;
4: out_red[31:24]<=8'D52;
5: out_red[31:24]<=8'D53;
6: out_red[31:24]<=8'D54;
7: out_red[31:24]<=8'D55;
8: out_red[31:24]<=8'D56;
9: out_red[31:24]<=8'D57;
10: out_red[31:24]<=8'D97;
11: out_red[31:24]<=8'D98;
12: out_red[31:24]<=8'D99;
13: out_red[31:24]<=8'D100;
14: out_red[31:24]<=8'D101;
15: out_red[31:24]<=8'D102;
endcase

case (hash[151:148])
0: out_red[23:16]<=8'D48;
1: out_red[23:16]<=8'D49;
2: out_red[23:16]<=8'D50;
3: out_red[23:16]<=8'D51;
4: out_red[23:16]<=8'D52;
5: out_red[23:16]<=8'D53;
6: out_red[23:16]<=8'D54;
7: out_red[23:16]<=8'D55;
8: out_red[23:16]<=8'D56;
9: out_red[23:16]<=8'D57;
10: out_red[23:16]<=8'D97;
11: out_red[23:16]<=8'D98;
12: out_red[23:16]<=8'D99;
13: out_red[23:16]<=8'D100;
14: out_red[23:16]<=8'D101;
15: out_red[23:16]<=8'D102;
endcase

case (hash[147:144])
0: out_red[15:8]<=8'D48;
1: out_red[15:8]<=8'D49;
2: out_red[15:8]<=8'D50;
3: out_red[15:8]<=8'D51;
4: out_red[15:8]<=8'D52;
5: out_red[15:8]<=8'D53;
6: out_red[15:8]<=8'D54;
7: out_red[15:8]<=8'D55;
8: out_red[15:8]<=8'D56;
9: out_red[15:8]<=8'D57;
10: out_red[15:8]<=8'D97;
11: out_red[15:8]<=8'D98;
12: out_red[15:8]<=8'D99;
13: out_red[15:8]<=8'D100;
14: out_red[15:8]<=8'D101;
15: out_red[15:8]<=8'D102;
endcase

case (hash[143:140])
0: out_red[7:0]<=8'D48;
1: out_red[7:0]<=8'D49;
2: out_red[7:0]<=8'D50;
3: out_red[7:0]<=8'D51;
4: out_red[7:0]<=8'D52;
5: out_red[7:0]<=8'D53;
6: out_red[7:0]<=8'D54;
7: out_red[7:0]<=8'D55;
8: out_red[7:0]<=8'D56;
9: out_red[7:0]<=8'D57;
10: out_red[7:0]<=8'D97;
11: out_red[7:0]<=8'D98;
12: out_red[7:0]<=8'D99;
13: out_red[7:0]<=8'D100;
14: out_red[7:0]<=8'D101;
15: out_red[7:0]<=8'D102;
endcase

//out_red[31:16]<=plain[39:24];
//out_red[15:0]<=plain[23:8];
//out_red[7:0]<=plain[7:0];

done<=1;
end
else done <= 0;
end
endmodule
