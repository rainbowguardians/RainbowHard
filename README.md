DE2_115_WEB_SERVER_RGMII_ENET0 - Hardware płytki (Verilog) + Software procesora (C)
FpgaClient - klient pozwalający na zdalną komunikację z płytką (Node.js)

Jak to działa?
1. Pobierz zawartość na swój komputer i rozpakuj
2. Jeśli masz płytkę (upewnij się że jest podłączona do komputera kablem USB i kablem ethernet do sieci):
    a) Uruchom program Quartus 15.1 i uruchom programator
    b) W programatorze wybierz plik 'DE2_115_WEB_SERVER_time_limited.sof' a następnie załaduj plik do płytki FPGA naciskając przysik start
    c) Po załadowaniu pliku wybierz z menu programu Quartus zakładkę 'tools' a z rozwiniętej listy 'Nios II Software Build Tools for Eclipse'
    d) W czasie ładowania programu Eclipse zostaniesz poproszony o wybranie 'workspace'. Znajdź i wybierz lokalizację 'DE2_115_WEB_SERVER_RGMII_ENET0/Software'
    e) Po uruchomieniu programu zobaczysz 4 foldery (tcpsocket, tcpsocket_bsp, web oraz web_bsp). Interesują nas tylko foldery tcpsocket...
    f) Naciśnij prawym przyciskiem myszy na folderze tcpsocket_bsp -> Nios II -> Generate BSP
    g) Po zakończeniu generacji rozwiń folder tcpsocket_bsp i otwórz system.h (dodaj tu następujące definicje: #define EXT_FLASH_BASE 0x9000000 oraz #define EXT_FLASH_NAME "/dev/cfi_flash")
    h) Zapisz zmieniony plik system.h (Ctrl + s) 
    i) Uruchom oprogramowanie procesora Nios 2. W tym celu naciśnij prawym przyciskiem myszy na folder tcpsocket -> Run As -> Nios II Hardware
    j) Po uruchomieniu programu zostanie wygenerowany plik z rozszerzeniem *.elf, zostanie on załadowany do pamięci urządzenia. W oknie konsoli zobaczysz procedurę nawiązywania połączenia. Po pewnym czasie zostanie wyświetlony lokalny adres IP oraz port nasłuchiwania płytki
3. Otwórz folder FpgaClient a następnie plik config.js
4. W otwartym pliku zmień adres IP localhost oraz port localport na wspomniany adres i port które zostały przypisane płytce (okno konsoli programu Eclipse)
5. Zapisz plik config.js i go zamknij.
6. Otwórz za pomocą konsoli oprogramowania Node.js plik app.js (ew przekieruj się w terminalu do folderu FpgaClient z plikiem app.js i wpisz w terminalu 'npm start')
7. Postępuj według wskazówek otwartego w oknie programu
